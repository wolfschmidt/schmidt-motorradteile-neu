<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$artfind = new Sms_Artikelfinder_Helper_Data();
$listhersteller = $artfind->getHersteller();
?>
<script>
    (function ($) {
        $(document).ready(function()
        {
            $("#json-one").change(function () {
                var $dropdown = $(this);
                $.getJSON("/media/sms/modelle.json", function (data) {
                    var key = $dropdown.val();
                    var vals = [];

                    $.each(data, function (k, v) {
                        if (key == k) {
                            vals = v.split(",");
                        }
                    })

                    var $jsontwo = $("#json-two");
                    $jsontwo.empty();
                    $.each(vals, function (index, value) {
                        $jsontwo.append("<option value='"+ value + "'>" + value + "</option>");
                    });
                });
            });
        })
    })(jQuery);
</script>


<div class="filter-sel" style="margin-top: 2px;">
    <select id="json-one" class="form-startseite" name="hersteller">
        <option value="" selected>[ Hersteller ]</option>
        <?php foreach ($listhersteller as $key => $value) : ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="filter-sel" style="margin-top: 2px; width: 200px;">
    <select id="json-two" class="form-startseite" name="hubraum">
        <option>[ Hubraum ]</option>
    </select>
</div>
<div class="filter-sel" style="margin-top: 2px; width: 200px;">
    <select id="json-three" class="form-startseite" name="type">
        <option>[ Typ ]</option>
    </select>
</div>
<div class="filter-sel" style="margin-top: 2px; width: 200px;">
    <select id="json-four" class="form-startseite" name="modell">
        <option>[ Fahrzeug ]</option>
    </select>
</div>

