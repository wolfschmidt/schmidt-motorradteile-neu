<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('shipping_matrixrate')}
  ADD `is_default` BOOLEAN NULL DEFAULT NULL AFTER `delivery_type`,
  ADD `sort_order` INT(4) NOT NULL DEFAULT '1000' AFTER `is_default`,
  ADD `forbidden_group` TEXT NOT NULL AFTER `sort_order`
   ");

$installer->endSetup();


