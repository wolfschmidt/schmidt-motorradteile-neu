<?php
class Sms_Artikelfinder_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
            $this->loadLayout();
            $this->getLayout()
                ->getBlock('root')
                ->setTemplate('page/1column.phtml');
            $this->renderLayout();
    }
    
    public function resultAction()
    {
            $this->loadLayout();
            $this->getLayout()
                ->getBlock('root')
                ->setTemplate('page/1column.phtml');
            $this->renderLayout();
    }
    
    public function artikelAction()
    {
            $this->loadLayout();
            $this->getLayout()
                ->getBlock('root')
                ->setTemplate('page/1column.phtml');
            $this->renderLayout();
    }
}