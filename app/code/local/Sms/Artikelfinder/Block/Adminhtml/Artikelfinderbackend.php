<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Sms_Artikelfinder_Block_Adminhtml_Artikelfinderbackend extends Mage_Adminhtml_Block_Template {

    public function __construct()
    {
        $this->_blockGroup = 'artikelfinder';
        $this->_controller = 'adminhtml_artikelfinder';
        $this->_headerText = $this->__('Grid');
         
        parent::__construct();
    }
}