<?php

$installer = $this;
$installer->startSetup();

$fahrzeug = $installer->getConnection()
        ->newTable($installer->getTable('sms_fahrzeug')) //this will select your table
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('fahrzeug_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Fahrzeug Id')
        ->addColumn('fahrzeugart', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Fahrzeugart')
        ->addColumn('hersteller', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Hersteller')
        ->addColumn('hubraum', Varien_Db_Ddl_Table::TYPE_TEXT, 5, array(
                ), 'Hubraum')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 10, array(
                ), 'Type')
        ->addColumn('modell', Varien_Db_Ddl_Table::TYPE_TEXT, 35, array(
                ), 'Modell')
        ->addColumn('bj_von', Varien_Db_Ddl_Table::TYPE_TEXT, 4, array(
                ), 'Baujahr von')
        ->addColumn('bj_bis', Varien_Db_Ddl_Table::TYPE_TEXT, 4, array(
                ), 'Baujahr bis')
        ->addColumn('fahrgestellnummer', Varien_Db_Ddl_Table::TYPE_TEXT, 8, array(
                ), 'Fahrgestellnummer')
        ->addIndex($installer->getIdxName('sms_fahrzeug', array('fahrzeug_id', 'herstller', 'modell')),
        array('fahrzeug_id', 'hersteller', 'modell'));

$installer->getConnection()->createTable($fahrzeug);

$artikel = $installer->getConnection()
        ->newTable($installer->getTable('sms_artikel_zu_fahrzeug')) //this will select your table
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('fahrzeug_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Fahrzeug Id')
        ->addColumn('artikelnr', Varien_Db_Ddl_Table::TYPE_TEXT, 35, array(
                ), 'Artikelnummer')
        ->addColumn('art_cat', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Kategorie')
        ->addColumn('art_subcat', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Unterkategorie')
        ->addIndex($installer->getIdxName('sms_artikel_zu_fahrzeug', array('fahrzeug_id', 'artikelnr')),
        array('fahrzeug_id', 'artikelnr'));

$installer->getConnection()->createTable($artikel);

$verwendung = $installer->getConnection()
        ->newTable($installer->getTable('sms_artikel_verwendung')) //this will select your table
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('fahrzeug_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Fahrzeug Id')
        ->addColumn('artikelnr', Varien_Db_Ddl_Table::TYPE_TEXT, 35, array(
                ), 'Artikelnummer')
        ->addColumn('verwendung', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Verwendung')
        ->addColumn('anzahl', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Anzahl')
        ->addColumn('fuellmenge', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
                ), 'Füllmenge')
        ->addIndex($installer->getIdxName('sms_artikel_zu_fahrzeug', array('fahrzeug_id', 'artikelnr')),
        array('fahrzeug_id', 'artikelnr'));

$installer->getConnection()->createTable($verwendung);

$installer->endSetup();
