<?php

class Sms_Artikelfinder_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getAllVehicles($param) {
        ($param['sort'] == "") ? $sort = "id" : $sort = $param['sort'];
        $value = 0;
        ($param['hersteller'] != "") ? $zahl = 1 : $zahl = 0;
        ($param['hubraum'] != "") ? $zahl2 = 2 : $zahl2 = 0;
        ($param['type'] != "") ? $zahl3 = 4 : $zahl3 = 0;
        ($param['modell'] != "") ? $zahl4 = 8 : $zahl4 = 0;
        $value = $zahl + $zahl2 + $zahl3 + $zahl4;

        switch ($value) {
            case 0:
                $whereclause = "";
                break;
            case 1:
                $whereclause = "WHERE hersteller like '$param[hersteller]'";
                break;
            case 2:
                $whereclause = "WHERE hubraum like '$param[hubraum]'";
                break;
            case 3:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND hubraum = '$param[hubraum]'";
                break;
            case 4:
                $whereclause = "WHERE type like '%$param[type]%'";
                break;
            case 5:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND hubraum = '%$param[type]%'";
                break;
            case 6:
                $whereclause = "WHERE hubraum = '$param[hubraum]' AND type like '%$param[type]%'";
                break;
            case 7:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND hubraum = '$param[hubraum]' AND type like '%$param[type]%'";
                break;
            case 8:
                $whereclause = "WHERE modell like '%$param[modell]%'";
                break;
            case 9:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND modell like '%$param[modell]%'";
                break;
            case 10:
                $whereclause = "WHERE hubraum like '$param[hubraum]' AND modell like '%$param[modell]%'";
                break;
            case 11:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND hubraum = '$param[hubraum]' AND modell like '%$param[modell]%'";
                break;
            case 12:
                $whereclause = "WHERE type like '$param[type]' AND modell like '%$param[modell]%'";
                break;
            case 13:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND type = '$param[type]' AND modell like '%$param[modell]%'";
                break;
            case 11:
                $whereclause = "WHERE type like '%$param[type]%' AND hubraum = '$param[hubraum]' AND modell like '%$param[modell]%'";
                break;
            case 15:
                $whereclause = "WHERE hersteller like '$param[hersteller]' AND hubraum = '$param[hubraum]' AND type like '%$param[type]%' AND modell like '%$param[modell]%'";
                break;
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_fahrzeug $whereclause ORDER BY sms_fahrzeug.$sort asc LIMIT 20";
        $fahrzeuge = $readConnection->fetchAll($SQL);
        return $fahrzeuge;
    }

    public function getVehicleIds($sku) {

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_artikel_zu_fahrzeug WHERE artikelnr = '$sku'";
        $ids = $readConnection->fetchAll($SQL);
        return $ids;
    }

    public function getVehicle($mkz) {

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_fahrzeug WHERE fahrzeug_id = '$mkz'";
        $fahrzeug = $readConnection->fetchRow($SQL);
        return $fahrzeug;
    }

    public function getStatus($fahrzeug, $sku) {

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT id FROM sms_artikel_zu_fahrzeug WHERE fahrzeug_id = '$fahrzeug' AND artikelnr like '$sku'";
        $data = $readConnection->fetchAll($SQL);
        if (count($data) > 0) {
            $result = "true";
        } else {
            $result = "false";
        }
        return $result;
    }

    public function addItemToVehicle($param) {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $cat = $param['cat'];
        $subcat = $param['subcat'];
        $sku = $param['sku'];
        $fahrzeug = $param['fahrzeug_id'];
        $vorhanden = $this->getStatus($fahrzeug, $sku);

        if ($vorhanden == "false") {
            $newresult = $writeConnection->query("INSERT INTO sms_artikel_zu_fahrzeug (fahrzeug_id, artikelnr, art_cat, art_subcat)
            VALUES
            ('$fahrzeug','$sku','$cat','$subcat');");
        } else {
            echo "bereits vorhanden";
//                $writeresult = $writeConnection->query(
//                        "UPDATE sms_modelpreise SET "
//                        . "taxi = '$_prices[taxi]',"
//                        . "in_out = '$in_out',"
//                        . "half = '$half',"
//                        . "one = '$one',"
//                        . "two = '$two',"
//                        . "sms_modelpreise.each = '$each',"
//                        . "overnight = '$overnight' WHERE id = $priceid");
        }
    }

    public function getVerwendung($fahrzeug, $sku) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT verwendung FROM sms_artikel_verwendung WHERE fahrzeug_id = '$fahrzeug' AND artikelnr like '$sku'";
        $data = $readConnection->fetchAll($SQL);

        return $data;
    }

    public function editVerwendung($param) {
        $fahrzeug = $param['fahrzeug'];
        $sku = $param['sku'];
        $aendern = $param['aendern'];
        $verwendung = $param['verwendung'];
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        if ($aendern == "entfernen") {
            $SQL = "DELETE FROM sms_artikel_verwendung WHERE fahrzeug_id = '$fahrzeug' AND artikelnr like '$sku' AND verwendung like '$verwendung' ";
        } elseif ($aendern == "hinzufuegen") {
            $SQL = "INSERT INTO sms_artikel_verwendung (fahrzeug_id, artikelnr, verwendung)"
                    . "VALUES"
                    . "('$fahrzeug', '$sku', '$verwendung')";
        }
        $result = $write->query($SQL);
    }

    public function getHersteller() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT hersteller FROM sms_fahrzeug ORDER BY hersteller";
        $result = $read->fetchCol($SQL);
        $result = array_unique($result);
        return $result;
    }

    public function getHubraumByHersteller($hersteller) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT hubraum FROM sms_fahrzeug WHERE hersteller like '$hersteller' ORDER BY hubraum";
        $result = $read->fetchCol($SQL);
        $result = array_unique($result);
        return $result;
    }

    public function getTypeByHh($hersteller, $hubraum) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT type FROM sms_fahrzeug WHERE hersteller like '$hersteller' AND hubraum = '$hubraum' ORDER BY type";
        $result = $read->fetchCol($SQL);
        $result = array_unique($result);
        return $result;
    }

    public function getModelle($hersteller, $hubraum, $type) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT modell, fahrzeug_id FROM sms_fahrzeug WHERE hersteller like '$hersteller' "
                . "AND hubraum = '$hubraum' AND type like '$type' GROUP BY modell ORDER BY modell";
        $result = $read->fetchAll($SQL);
        return $result;
    }

    public function getBikes($modell) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_fahrzeug WHERE modell like '$modell' ";
        $result = $read->fetchAll($SQL);
        return $result;
    }
    
    public function getBike($mkz) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_fahrzeug WHERE fahrzeug_id = '$mkz' LIMIT 1";
        $result = $read->fetchRow($SQL);
        return $result;
    }

    public function getItemsByModell($mkz) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $SQL = "SELECT * FROM sms_artikel_zu_fahrzeug WHERE fahrzeug_id like '$mkz' ";
        $result = $read->fetchAll($SQL);
        return $result;
    }

    public function getCategories($artikel) {
        $categories = array();
        foreach ($artikel as $item) {
            foreach ($item as $key => $value) {
//            echo $key." => ".$value."<br>";
                if ($key == "art_cat") {
                    if($value == "") { $value="Sonstiges"; }
                    array_push($categories, $value);
                }
            }
        }
        $categories = array_unique($categories);
        asort($categories);
        return $categories;
    }

}
