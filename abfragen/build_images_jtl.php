<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 04.08.2020
 * Time: 10:18
 */
$start = microtime(true);
$class = NEW SmsConvertArtikelexport();

define('IMPORT_FILE', 'artikelnummern.csv');
define('IMPORT_PATH', '');
define('TRENNER', ';');
define('BILDPFAD', 'F:\\Ablage\\Motorradservice Schmidt\\Warenwirtschaft\\Lieferanten\\Bilder PaWu\\');
$fileRowCount = 0;
$loeschen = "sku_images.csv";
if (file_exists($loeschen)) {
    unlink($loeschen);
    } else {
echo "Die Datei $loeschen existiert nicht.<br>";
}
$attr = fopen("sku_images.csv", 'w');
if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $zeile = $data[0].TRENNER.BILDPFAD.$data[0].".jpg".TRENNER.BILDPFAD.$data[0]."_1.jpg".TRENNER.BILDPFAD.$data[0]."_2.jpg".TRENNER.BILDPFAD.$data[0]."_3.jpg\n";
        echo $zeile."<br>";
        fwrite($attr, $zeile);
    }

    $class->closeFile($fileHandle);
}

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
