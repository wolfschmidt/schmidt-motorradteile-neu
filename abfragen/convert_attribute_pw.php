<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 23.10.2017
 * Update 17.10.2017
 * Time: 11:18
 */
$start = microtime(true);
$class = NEW SmsConvertAttribute();


define('IMPORT_FILE', '\artikelexport_v2_zubehoer.csv');
define('IMPORT_PATH', 'F:\Ablage\Motorradservice Schmidt\Warenwirtschaft\Lieferanten');
define('ESCAPE', '"');
define('TRENNER', ';');

$attr = fopen("attribute_pw.csv", 'w');
$zeile = ESCAPE . "sku" . ESCAPE . TRENNER . ESCAPE . "attribute" . ESCAPE . TRENNER . ESCAPE . "value" . ESCAPE . "\n";
//fwrite($attr, ESCAPE . "sku" . ESCAPE . TRENNER . ESCAPE . "attribute" . ESCAPE . TRENNER . ESCAPE . "value" . ESCAPE . "\n");

$fileRowCount = 0;

echo IMPORT_PATH . IMPORT_FILE . "<br>";
//exit;
if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {

        $fileRowCount++;
        if ($fileRowCount > 1) {
            if (!empty($data[0])) {
//            $zeile = $data[0].TRENNER;
                if (!empty($data[3])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "OEM Nummer (nur zu Vergleichszwecken)" . ESCAPE . TRENNER . ESCAPE . $data[3] . ESCAPE . "\n";
                }
                if (!empty($data[14])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Material" . ESCAPE . TRENNER . ESCAPE . $data[14] . ESCAPE . "\n";
                }
                if (!empty($data[15])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Farbe" . ESCAPE . TRENNER . ESCAPE . $data[15] . ESCAPE . "\n";
                }
                if (!empty($data[17])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Gutachten" . ESCAPE . TRENNER . ESCAPE . $data[17] . ESCAPE . "\n";
                }
                if (!empty($data[18])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Fahrzeugmarke" . ESCAPE . TRENNER . ESCAPE . $data[18] . ESCAPE . "\n";
                }
                if (!empty($data[25])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Zähneanzahl" . ESCAPE . TRENNER . ESCAPE . $data[25] . ESCAPE . "\n";
                }
                if (!empty($data[26])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Kettenglieder" . ESCAPE . TRENNER . ESCAPE . $data[26] . ESCAPE . "\n";
                }
                if (!empty($data[27])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Kettenlänge" . ESCAPE . TRENNER . ESCAPE . $data[27] . ESCAPE . "\n";
                }
                if (!empty($data[28])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Kettenteilung" . ESCAPE . TRENNER . ESCAPE . $data[28] . ESCAPE . "\n";
                }
                if (!empty($data[30])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Ausführung" . ESCAPE . TRENNER . ESCAPE . $data[30] . ESCAPE . "\n";
                }
                if (!empty($data[31])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Durchmesser" . ESCAPE . TRENNER . ESCAPE . $data[31] . ESCAPE . "\n";
                }
                if (!empty($data[32])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Gewinde" . ESCAPE . TRENNER . ESCAPE . $data[32] . ESCAPE . "\n";
                }
                if (!empty($data[33])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Innendurchmesser" . ESCAPE . TRENNER . ESCAPE . $data[33] . ESCAPE . "\n";
                }
                if (!empty($data[34])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Innendurchmesser" . ESCAPE . TRENNER . ESCAPE . $data[34] . ESCAPE . "\n";
                }
                if (!empty($data[35])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Breite" . ESCAPE . TRENNER . ESCAPE . str_replace(".",",",$data[35]) . " mm" . ESCAPE . "\n";
                }
                if (!empty($data[36])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Höhe" . ESCAPE . TRENNER . ESCAPE . $data[35] . " mm" . ESCAPE . "\n";
                }
                if (!empty($data[37])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Dicke" . ESCAPE . TRENNER . ESCAPE . $data[37] . " mm" . ESCAPE . "\n";
                }
                if (!empty($data[38])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Länge" . ESCAPE . TRENNER . ESCAPE . $data[38] . " mm" . ESCAPE . "\n";
                }
                if (!empty($data[39])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Tiefe" . ESCAPE . TRENNER . ESCAPE . $data[39] . " mm" . ESCAPE . "\n";
                }
                if (!empty($data[40])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Form" . ESCAPE . TRENNER . ESCAPE . $data[40] . ESCAPE . "\n";
                }
                if (!empty($data[41])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Füllmenge" . ESCAPE . TRENNER . ESCAPE . $data[41] . ESCAPE . "\n";
                }
                if (!empty($data42)) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Glas" . ESCAPE . TRENNER . ESCAPE . $data[42] . ESCAPE . "\n";
                }
                if (!empty($data[44])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Kapazität" . ESCAPE . TRENNER . ESCAPE . $data[44] . ESCAPE . "\n";
                }
                if (!empty($data[45])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Leuchtmittel" . ESCAPE . TRENNER . ESCAPE . $data[45] . ESCAPE . "\n";
                }
                if (!empty($data[46])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Verstellerfarbe" . ESCAPE . TRENNER . ESCAPE . $data[46] . ESCAPE . "\n";
                }
                if (!empty($data[47])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Verwendung" . ESCAPE . TRENNER . ESCAPE . $data[47] . ESCAPE . "\n";
                }
                if (!empty($data[48])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Volt" . ESCAPE . TRENNER . ESCAPE . $data[48] . ESCAPE . "\n";
                }
                if (!empty($data[49])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Watt" . ESCAPE . TRENNER . ESCAPE . $data[49] . ESCAPE . "\n";
                }
                if (!empty($data[53])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Höherlegung" . ESCAPE . TRENNER . ESCAPE . $data[51] . ESCAPE . "\n";
                }
                if (!empty($data[54])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Anschluss" . ESCAPE . TRENNER . ESCAPE . $data[52] . ESCAPE . "\n";
                }
                if (!empty($data[55])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Batteriehöhe" . ESCAPE . TRENNER . ESCAPE . $data[53] . ESCAPE . "\n";
                }
                if (!empty($data[56])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Batterietiefe" . ESCAPE . TRENNER . ESCAPE . $data[54] . ESCAPE . "\n";
                }
                if (!empty($data[57])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Befestigung" . ESCAPE . TRENNER . ESCAPE . $data[55] . ESCAPE . "\n";
                }
                if (!empty($data[59])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Lochkreis" . ESCAPE . TRENNER . ESCAPE . $data[57] . ESCAPE . "\n";
                }
                if (!empty($data[60])) {
                    $zeile = $zeile . ESCAPE . $data[0] . ESCAPE . TRENNER . ESCAPE . "Anzahl Bohrungen" . ESCAPE . TRENNER . ESCAPE . $data[58] . ESCAPE . "\n";
                }
                
//            if ($data[1] == utf8_decode("Glas")) {
//                fwrite($glas, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
//                echo $data[0] . TRENNER . $data[2] . "<br>";
//            }
//                $zeile = $zeile . "\n";
                echo $fileRowCount."<br>";
                
            }
        }
    }
    fwrite($attr, $zeile);
}

$end = microtime(true);

$runtime = number_format($end - $start, 2);
$fileRowCount = $fileRowCount - 1;
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->printMessage($message);

class SmsConvertAttribute {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
