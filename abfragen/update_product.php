<?php

/**
 * Created by PhpStorm.
 * User: STENDEL, Wolf Schmidt - Schmidt Medienservice
 * Date: 20.04.2016
 * Time: 11:18
 */
require_once '../app/Mage.php';
Mage::app();

$start = microtime(true);
$class = NEW StendelUpdateProducte();

//ini_set('memory_limit','4096M');
ini_set('display_errors', '1');
error_reporting(1);

define('IMPORT_FILE', 'jtl.csv');
define('CLEAR_FILE', 'jtl-inaktiv.csv');
define('ATTRIBUTE_EXPORT', 'jtl-attribute.csv');
define('IMPORT_PATH', 'abfragen/');

define('LOG_FILE', 'product_update.log');
define('LOG_PATH', 'var/log/');

$fileRowCount = 0;
$actionCount = 0;

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//$de = Mage::app()->getStore('brands4bikes_de')->getId();
//$en = Mage::app()->getStore('brands4bikes_en')->getId();
//$pl = Mage::app()->getStore('brands4bikes_pl')->getId();
$logFileHandle = $class->openFile('../' . LOG_PATH, LOG_FILE, 'a+');

$cat = Mage::getModel('catalog/category');
$catTree = $cat->getTreeModel()->load();
$catIds = $catTree->getCollection()->getAllIds();


// Als erstes Shop-Artikel bereinigen
// ------------------------------------------------------------------------------------------------------------------------------------

if (($fileHandle = $class->openFile(Mage::getBaseDir() . '/' . IMPORT_PATH, CLEAR_FILE, 'r')) !== FALSE) {
    $countActive = 0;
    $countInactive = 0;
    $inactiveProducts = array();
    while (($inactive = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $theDeleteObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($inactive[0]));
        if ($theDeleteObj->getSku() != "") {
            $message = $theDeleteObj->getSku() . " fliegt raus";
            $class->printMessage($message);

            $theDeleteObj->delete();
            $countInactive++;
        } else {
            $countActive++;
        }
    }
}

$end = microtime(true);
$runtime = number_format($end - $start, 2);

$message = "Laufzeit für die Bereinigung [" . number_format($end - $start, 2) . "] Sekunden. " . $countActive .
        " sind nicht vorhanden konnten nicht gelöscht werden. <br><br>\n\r";
$class->printMessage($message);

// Ab hier die Attribute - erst den Export formatiern, dann in ein produktabhängiges Array schreiben
$class->truncateTable();

if (($attributeHandle = $class->openFile(Mage::getBaseDir() . '/' . IMPORT_PATH, ATTRIBUTE_EXPORT, 'r')) !== FALSE) {
    while (($_attributes = fgetcsv($attributeHandle, 1000, ";")) !== FALSE) {
        $class->insertSmsAttribute($_attributes[0], $_attributes[1], $_attributes[2]);
    }
}
$class->closeFile($attributeHandle);


// Danach update vorhandener Produkte
// ----------------------------------------------------------------------------------------------------------------------------------
if (($fileHandle = $class->openFile(Mage::getBaseDir() . '/' . IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 0, ";")) !== FALSE) {
        $_needCatRefresh = false;
        $fileRowCount++;
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

//        if ($data[30] == "0.0000") { $data[30] = ""; }
        if ($data[27] === "30.12.1899") {
            $von = "01.01.1970";
        } else {
            $von = $data[27];
        }
        if ($data[28] === "30.12.1899") {
            $bis = "02.01.1970";
        } else {
            $bis = $data[28];
        }
        if ($data[11] == "0.00") {
            $data[11] = 2.5;
        }
        if ($data[12] == "0.00") {
            $data[12] = 3;
        }
        $neu_bis = strtotime("+30 day", strtotime($data[30]));
        $neu_bis = date("d.m.Y", $neu_bis);

        $fileData = array(
            'sku' => str_replace('"', '', $data[0]),
            'han' => $data[1],
            'name' => $data[2],
            'shortdesc' => $data[3],
            'description' => $data[4],
            'metatitle' => $data[5],
            'metakeywords' => $data[6],
            'metadescription' => $data[7],
            'preisbrutto' => $data[8],
            'uvp' => $data[9],
            'steuerklasse' => $data[10],
            'verf_menge' => $data[12],
            'gewicht' => $data[11],
            'hersteller' => $data[13],
            'barcode' => $data[14],
            'herkunftsland' => $data[15],
            'lieferstatus' => $data[16],
            'neu_von' => $data[30],
            'neu_bis' => $neu_bis,
            'sonderpreis' => $data[29],
            'sp_von' => date_format(date_create($von), 'Y-m-d H:i:s'),
            'sp_bis' => date_format(date_create($bis), 'Y-m-d H:i:s'),
//            'bild1' => $class->extrudeImage($data[11]),
//            'bild2' => $class->extrudeImage($data[12]),
//            'bild3' => $class->extrudeImage($data[13]),
//            'bild4' => $class->extrudeImage($data[14]),
        );

        echo $fileRowCount . '-> SKU: ' . $fileData['sku'] . ' - ' . $fileData['description'] . "<br>\r\n";

        $_updatedSaved = 0;
        $_attributeUpdatedSaved = 0;
        $_languageUpdatedSaved = 0;

        $_categories = array();
        for ($i = 17; $i < 27; $i++) {
            $_category = Mage::getResourceModel('catalog/category_collection')
                    ->addFieldToFilter('name', utf8_encode($data[$i]))
                    ->getFirstItem();

            $categoryId = $_category->getId();
            if ($categoryId != "") {
                array_push($_categories, $categoryId);
            }
        }

        $isInStock = 1;
        $sichtbar = 4;

        $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($fileData['sku']));
        if ($theProductObj->getSku() == $fileData['sku']) {

//Globale Daten
//            if ($theProductObj->getStatus() != $fileData['blocked']) {
//                Mage::getModel('catalog/product_status')->updateProductStatus($theProductObj->getId(), 0, $fileData['blocked']);
//                $_updatedSaved++;
//           }
//           // Hier der Bilder vergleich (muss für pwonline noch geändert werden
//            if ($theProductObj->getImageExternalUrl() != $fileData['bild1']) :
//                $theProductObj
//                    ->setImageExternalUrl($fileData['bild1'])
//                    ->setSmallImageExternalUrl($fileData['bild1'])
//                    ->setThumbnailExternalUrl($fileData['bild1']);
//                $_needCatRefresh = true;
//                $_updatedSaved++;
//            endif;
//            if ($theProductObj->getExternalGallery() != $_gallery) {
//                $theProductObj->setExternalGallery($_gallery);
//                $_updatedSaved++;
//            }
            if ($theProductObj->getBarcode() != $fileData['barcode']) {
                $theProductObj->setBarcode($fileData['barcode']);
                $_updatedSaved++;
            }

            if ($theProductObj->getQty() != $fileData['verf_menge']) {
                $theProductObj->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'is_in_stock' => $isInStock, //Stock Availability
                    'qty' => $fileData['verf_menge'] //qty
                ));
                $_updatedSaved++;
            }

//            Prüfen ob exportierte Kategorie vorhanden und gesettz
            foreach ($_categories as $_categoryId) {
                if ($_categoryId != 0) :
                    $categoryIds = $theProductObj->getCategoryIds();
                    if (!in_array($_categoryId, $categoryIds)) :
                        if (in_array($_categoryId, $catIds)):
                            $theProductObj->setCategoryIds(array($_categories));
                            $_updatedSaved++;
                        endif;
                    endif;
                endif;
            }

//            Prüfen ob gesetzte Kategorien noch gültig
            $categoryIds = $theProductObj->getCategoryIds();
            foreach ($categoryIds as $_categoryId) {
                if (!in_array($_categoryId, $_categories)) :
                    $theProductObj->setCategoryIds(array($_categories));
                    $_updatedSaved++;
                endif;
            }

            if ($theProductObj->getWeight() != $fileData['gewicht']) {
                $theProductObj->setWeight($fileData['gewicht']);
                $_updatedSaved++;
            }

            if ($theProductObj->getCountryOfManufacture() != $fileData['herkunftsland']) {
                $theProductObj->setCountryOfManufacture($fileData['herkunftsland']);
                $_updatedSaved++;
            }

            if ($theProductObj->getVisibility() != $sichtbar) {
                $theProductObj->setVisibility($sichtbar);
                $_updatedSaved++;
            }

            if ($theProductObj->getPrice() != $fileData['preisbrutto']) {
                $theProductObj->setPrice($fileData['preisbrutto']);
                $_updatedSaved++;
            }

            if ($theProductObj->getSpecialPrice() != $fileData['sonderpreis']) {
                $theProductObj->setSpecialPrice($fileData['sonderpreis']);
                $_updatedSaved++;
            }

            if ($theProductObj->getSpecialToDate() != $fileData['sp_bis']) {
                $theProductObj
                        ->setSpecialFromDate($fileData['sp_von'])
                        ->setSpecialToDate($fileData['sp_bis']);
                $_updatedSaved++;
            }

            if ($theProductObj->getName() != $fileData['name'] OR $theProductObj->getShortDescription() != $fileData['shortdesc'] OR $theProductObj->getDescription() != $fileData['description']) {
                $theProductObj
                        ->setStoreId(0)
                        ->setName($fileData['name'])
                        ->setShortDescription($fileData['shortdesc'])
                        ->setDescription($fileData['description'])
                        ->setUpdatedAt(strtotime('now'));
                $_updatedSaved++;
            }

            if ($_updatedSaved > 0) {
                $theProductObj->save();
            } else {
                $message = "Keine allgemeinen Änderungen an " . $fileData['sku'] . "<br>";
                $class->printMessage($message);
            }

            $attributes = $class->getAttributes($theProductObj->getSku());
            echo count($attributes)." Attribute für ".$fileData['sku']."<br>";
            print_r($attributes);
//            foreach ($attributes as $_attributes) {
//                foreach ($_attributes as $key => $value) {
//                    echo $key . " => " . $value . "<br>";
//                }
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_gutachten') != $fileData['gutachten']) {
//                $class->setAttributeOption('shopsync_filter_gutachten', $fileData['gutachten'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "gutachten";
//            }
            if ($_attributeUpdatedSaved > 0) {
                $message = $fileData['sku'] . " " . $_attributeUpdatedSaved . " Attribute geändert";
                $class->printMessage($message);
                $message = "Das geänderte Attribut ist " . $_attributeName;
                $class->printMessage($message);
            } else {
                $message = $fileData['sku'] . " ohne Attributänderungen";
                $class->printMessage($message);
            }
// --------------------------------------------------------------------------------------------------------------------------------
//Storeabhängige Daten
            //Ab hier die sprachabhängige Artikeltext-Synchronisation
//            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//            $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($fileData['sku']));
//            if ($theProductObj->getName() != $fileData['name'] OR $theProductObj->getShortDescription() != $fileData['shortdesc'] OR $theProductObj->getDescription() != $fileData['description']) :
//                $theProductObj
//                        ->setStoreId(0)
//                        ->setName($fileData['name'])
//                        ->setShortDescription($fileData['shortdesc'])
//                        ->setDescription($fileData['description'])
//                        ->setUpdatedAt(strtotime('now'))
//                        ->save();
//                $_languageUpdatedSaved++;
//            endif;
//
//            if ($_languageUpdatedSaved > 0) {
//                $message = $fileData['sku'] . " Sprachen aktualisiert<br>";
//            } else {
//                $message = $fileData['sku'] . " ohne Sprachänderungen<br>";
//            }
        } else { // Neues Produkt anlegen
            $message = $fileData['sku'] . " als neues Produkt anlegen<br>";
            $class->printMessage($message);

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel('catalog/product');

            $product
                    ->setWebsiteIds(array(1))//website ID the product is assigned to, as an array
                    ->setAttributeSetId(4)//ID of a attribute set named 'default'
                    ->setTypeId('simple')//product type
                    ->setCreatedAt(strtotime('now'))//product creation time
                    ->setSku($fileData['sku'])//SKU
                    ->setName($fileData['name'])//product name
                    ->setWeight($fileData['gewicht'])
                    ->setStatus(1)//product status (1 - enabled, 2 - disabled)
                    ->setCountryOfManufacture($fileData['herkunftsland'])
                    ->setNewsFromDate($fileData['neu_von'])
                    ->setNewsToDate($neu_bis)
                    ->setTaxClassId(2)//tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility($sichtbar)//catalog and search visibility
                    ->setPrice($fileData['preisbrutto'])//price in form 11.22
                    ->setSpecialPrice($fileData['sonderpreis'])//special price in form 11.22
                    ->setSpecialFromDate($fileData['sp_von'])//special price from (MM-DD-YYYY)
                    ->setSpecialToDate($fileData['sp_bis'])//special price to (MM-DD-YYYY)
                    ->setDescription($fileData['description'])
                    ->setShortDescription($fileData['shortdesc'])
                    ->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock' => 1, //manage stock
                        'is_in_stock' => $isInStock, //Stock Availability
                        'qty' => $fileData['verf_menge'] //qty
                            )
                    )
                    ->setCategoryIds(array($_categories)) //assign product to categories
//                    ->setImageUrl($fileData['bild1'])
//                    ->setSmallImageUrl($fileData['bild1'])
//                    ->setThumbnailUrl($fileData['bild1'])
//                    ->setGallery($_gallery)
            ;
            $product->save();
//            if (!empty($fileData['ausfuehrung'])) {
//                $class->setAttributeOption('shopsync_filter_ausfuehrung', $fileData['ausfuehrung'], $product);
//            }
//            if (!empty($fileData['fahrzeughersteller'])) {
//                $class->setAttributeOption('shopsync_filter_fahrzeugmarke', $fileData['fahrzeughersteller'], $product);
//            }
//            if (!empty($fileData['farbe'])) {
//                $class->setAttributeOption('shopsync_filter_farbe', $fileData['farbe'], $product);
//            }
//            if (!empty($fileData['gutachten'])) {
//                $class->setAttributeOption('shopsync_filter_gutachten', $fileData['gutachten'], $product);
//            }
//            if (!empty($fileData['material'])) {
//                $class->setAttributeOption('shopsync_filter_material', $fileData['material'], $product);
//            }
//            if (!empty($fileData['produktmarke'])) {
//                $class->setAttributeOption('shopsync_filter_produktmarke', $fileData['produktmarke'], $product);
//            }
        }
    }
    $end2 = microtime(true);
    $message2 = "Laufzeit gesamt inklusive Bereinigung [" . number_format($end2 - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen und " . $actionCount . " Aenderungen vorgenommen.";
    $class->printMessage($message2);
    $class->closeFile($fileHandle);
} else {
    $message = "Keine Input-Datei gefunden!!!";
    $class->printMessage($message);
}

class StendelUpdateProducte {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo $message . "\n";
    }

    /**
     * @param $image
     * @return string
     */
    public function extrudeImage($image) {
        $needle = "\\";

        if ($image == "(Leer)") {
            $image = "";
        } else {
            $pos = strripos($image, $needle);
            if ($pos != 0) {
                $pos = $pos + 1;
            }
            $image = substr($image, $pos);
        }
        return $image;
    }

    /**
     * @param $attribute_code
     * @param $data
     * @param $theProductObj
     */
    public function setAttributeOption($attribute_code, $data, $theProductObj) {
        $attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute_code);
        $attribute_value_id = $attribute->getSource()->getOptionId($data);
        $theProductObj->addAttributeUpdate($attribute_code, $attribute_value_id);
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getCurlUrl($url) {
        $allStores = Mage::app()->getStores();
        $agent = 'User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:23.0) Gecko/20100101 Firefox/45.0';

        $scriptName = $_SERVER['SCRIPT_NAME'];
        $url = preg_replace("[$scriptName/]", "", $url);

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $message = 'Refreshing Category Url [' . Mage::app()->getStore()->getCode() . ' - ' . $url . ']';
        $this->printMessage($message);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_MUTE, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_exec($ch);

        foreach ($allStores as $eachStoreId => $val) {
            Mage::app()->setCurrentStore($eachStoreId);
            $message = 'Refreshing Category Url [' . Mage::app()->getStore()->getCode() . ' - ' . $url . ']';
            $this->printMessage($message);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_MUTE, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_exec($ch);
        }
    }

    /**
     * @param $categoryId
     * @param $productId
     * @param $newPosition
     */
    public function setPosition($categoryId, $productId, $newPosition) {
        $category = Mage::getModel('catalog/category')->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($categoryId);
        $myProduct = $category->getProductsPosition();
        $myProduct[$productId] = $newPosition;
        $category->setPostedProducts($myProduct);
        $category->save();
    }

    public function truncateTable() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "schmidt_motorradteile";
        $conn = mysqli_connect($servername, $username, $password, $dbname);

        $sql = "TRUNCATE TABLE sms_attribute";
        if (mysqli_query($conn, $sql)) {
            echo "sms_attribute geleert<br>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

        mysqli_close($conn);
    }

    public function insertSmsAttribute($sku, $key, $value) {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "schmidt_motorradteile";
        $conn = mysqli_connect($servername, $username, $password, $dbname);

        $sql = "INSERT INTO sms_attribute (id, sku, attribut, value) VALUES (NULL, '$sku', '$key', '$value')";
        if (mysqli_query($conn, $sql)) {
            echo "New record for " . $sku . " created successfully: " . $key . " => " . $value . "<br>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }

    public function getAttributes($sku) {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "schmidt_motorradteile";
        $conn = mysqli_connect($servername, $username, $password, $dbname);

        $sql = "SELECT * from sms_attribute WHERE sku = '$sku'";
        if (mysqli_query($conn, $sql)) {
            $attributes = mysqli_fetch_all($conn->query($sql));
            return($attributes);
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }

}
