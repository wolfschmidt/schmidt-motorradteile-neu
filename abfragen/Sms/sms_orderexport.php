<?php

/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 2015-05-20
 * alle Rechte liegen beim Autor
 */
ini_set("memory_limit", '725M');
//ini_set('max_execution_time', 900);
ini_set('display_errors', 1);
require_once '../../app/Mage.php';
$class = NEW SmsOrderExport();
Mage::app();

$orders = $class->getOrders();

$customer = fopen(Mage::getBaseDir() . "/sales/customer_export.csv", "w+");
$csvHeader = array(
    'Customer ID',
    'Customer Email',
    'Customer Prefix',
    'Customer First Name',
    'Customer Last Name',
    'Customer Phone',
    'Customer City',
    'Customer Postcode',
    'Customer Street',
    'Customer Country Id',
    'Payment Mehtod',
    'Shipping Email',
    'Shipping Prefix',
    'Shipping First Name',
    'Shipping Last Name',
    'Shipping Phone',
    'Shipping City',
    'Shipping Postcode',
    'Shipping Street',
    'Shipping Country Id');
fputcsv($customer, $csvHeader, ";");

foreach ($orders as $order) {
    $billing = $class->getBillingAddress($order['billing_address_id']);
    $shipping = $class->getShippingAddress($order['shipping_address_id']);
    $payment = $class->getPayment($order['entity_id']);

    if ($payment[0]['method'] == "banktransfer") {
        $paymentmethod = "Überweisung";
    } elseif ($payment[0]['method'] == "paypal_express") {
        $paymentmethod = "PayPal";
    }
    $_customer = array(
        $billing[0]['customer_id'],
        $billing[0]['email'],
        $billing[0]['prefix'],
        $billing[0]['firstname'],
        $billing[0]['lastname'],
        $billing[0]['telephone'],
        $billing[0]['city'],
        $billing[0]['postcode'],
        $billing[0]['street'],
        $billing[0]['country_id'],
        $paymentmethod,
        $shipping[0]['email'],
        $shipping[0]['prefix'],
        $shipping[0]['firstname'],
        $shipping[0]['lastname'],
        $shipping[0]['telephone'],
        $shipping[0]['city'],
        $shipping[0]['postcode'],
        $shipping[0]['street'],
        $shipping[0]['country_id']
    );
    fputcsv($customer, $_customer, ";");
}

fclose($customer);

// ab hier die Bestellungen
$importorders = fopen(Mage::getBaseDir() . "/sales/order_export.csv", "w+");
$orderHeader = array(
    'Customer ID',
    'Order ID',
    'Customer Email',
    'Customer Prefix',
    'Customer First Name',
    'Customer Last Name',
    'Customer Phone',
    'Customer City',
    'Customer Postcode',
    'Customer Street',
    'Customer Country Id',
    'Payment Mehtod',
    'Shipping Email',
    'Shipping Prefix',
    'Shipping First Name',
    'Shipping Last Name',
    'Shipping Phone',
    'Shipping City',
    'Shipping Postcode',
    'Shipping Street',
    'Shipping Country Id',
    'Positionstyp',
    'SKU',
    'Name',
    'Amount',
    'Price',
    'Tax',
    'Discount',
    'Versandart');
fputcsv($importorders, $orderHeader, ";");

foreach ($orders as $order) {
    $billing = $class->getBillingAddress($order['billing_address_id']);
    $shipping = $class->getShippingAddress($order['shipping_address_id']);
    $payment = $class->getPayment($order['entity_id']);
    $items = $class->getOrderItems($order['entity_id']);

    if ($order['shipping_description'] == "Bitte Versandart wählen - DHL Standard") {
        $shippingmethod = "DHL deutschlandweit";
    } elseif ($order['shipping_description'] == "Bitte Versandart wählen - Hermes Versand") {
        $shippingmethod = "Hermes Paket Inland";
    } else {
        $shippingmethod = "Briefversand";
    }
    echo $shippingmethod . "<br>";

    echo $items[0]['sku'] . "<br>";
    foreach ($items as $item) {
        $_order = array(
            $order['customer_id'],
            $order['increment_id'],
            $order['customer_email'],
            $billing[0]['prefix'],
            $billing[0]['firstname'],
            $billing[0]['lastname'],
            $billing[0]['telephone'],
            $billing[0]['city'],
            $billing[0]['postcode'],
            $billing[0]['street'],
            $billing[0]['country_id'],
            $paymentmethod,
            $shipping[0]['email'],
            $shipping[0]['prefix'],
            $shipping[0]['firstname'],
            $shipping[0]['lastname'],
            $shipping[0]['telephone'],
            $shipping[0]['city'],
            $shipping[0]['postcode'],
            $shipping[0]['street'],
            $shipping[0]['country_id'],
            'Artikel',
            $item['sku'],
            $item['name'],
            $item['qty_ordered'],
            $item['original_price'],
            $item['tax_percent'],
            $item['discount_percent'],
            $shippingmethod
        );
        fputcsv($importorders, $_order, ";");
    }

    $class->setOrderStatus($order['entity_id']);
}


fclose($importorders);

class SmsOrderExport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    public function getOrders() {
        $dbRead = $this->connect("core_read");
        $SQL = "SELECT entity_id, increment_id, billing_address_id, shipping_address_id, shipping_description, customer_email, shipping_description "
                . "FROM sales_flat_order "
                . "WHERE STATE like 'new' OR STATE like 'processing'";
        $orders = $dbRead->fetchAll($SQL);
        return $orders;
    }

    public function getBillingAddress($id) {
        $dbRead = $this->connect("core_read");
        $_billing = "SELECT customer_id, prefix, firstname, lastname, street, postcode, city, telephone, fax, email, country_id FROM sales_flat_order_address"
                . " WHERE entity_id = " . $id;
        $billing = $dbRead->fetchAll($_billing);
        return $billing;
    }

    public function getShippingAddress($id) {
        $dbRead = $this->connect("core_read");
        $_shipping = "SELECT prefix, firstname, lastname, street, postcode, city, telephone, fax, email, country_id FROM sales_flat_order_address"
                . " WHERE entity_id = " . $id;
        $shipping = $dbRead->fetchAll($_shipping);
        return $shipping;
    }

    public function getPayment($id) {
        $dbRead = $this->connect("core_read");
        $_payment = "SELECT method FROM sales_flat_order_payment WHERE parent_id = " . $id;
        $payment = $dbRead->fetchAll($_payment);
        return $payment;
    }

    public function getOrderItems($orderId) {
        $dbRead = $this->connect("core_read");
        $_items = "SELECT sku, name, qty_ordered, original_price, tax_percent, discount_percent FROM sales_flat_order_item WHERE order_id = $orderId";
        $items = $dbRead->fetchAll($_items);
        return $items;
    }

    public function setOrderStatus($oId) {
        $dbWrite = $this->connect("core_write");
        $writeresult = $dbWrite->query("UPDATE sales_flat_order SET state = 'complete', status='complete' WHERE entity_id = $oId");
        $writeresult = $dbWrite->query("UPDATE sales_flat_order_grid SET status = 'complete' WHERE entity_id = $oId");
    }

    private function connect($kind) {
        $coreResource = Mage::getSingleton('core/resource');
        $dbWrite = $coreResource->getConnection('core_write');
        $dbRead = $coreResource->getConnection('core_read');
        $con = $coreResource->getConnection($kind);
        return $con;
    }

}
