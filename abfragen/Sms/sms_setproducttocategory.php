<?php

/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 2015-05-20
 * alle Rechte liegen beim Autor
 */
ini_set("memory_limit", '256M');
ini_set('max_execution_time', 900);
require_once '../../app/Mage.php';

UMASK(0);
Mage::app();
Mage::init();

$class = NEW SmsConvertArtikelexport();
$_helper = Mage::helper('catalog/category');

Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
error_reporting(1);

define('IMPORT_FILE', 'Artikelkategorien.csv');
define('IMPORT_PATH', '../../var/import/');

$zeilen = file(IMPORT_PATH . IMPORT_FILE);

$temp = array();
$skus = array();
$i = 0;
$zeiger = 0;
if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $_categories = array();
        $spalte = 1;
        if ($zeiger > 0) {
            array_push($skus, $data[0]);
            while ($spalte <= 4) {

                if ($data[$spalte] != "" && $spalte == 1) {
                    $_category = Mage::getResourceModel('catalog/category_collection')
                            ->addFieldToFilter('name', utf8_encode($data[$spalte]))
                            ->getFirstItem();

                    $categoryId = $_category->getId();
                    if ($categoryId != "") {
                        $temp[$i] = array($data[0] => $categoryId);
                    }
                } elseif ($data[$spalte] != "" && $spalte > 1) {
                    // neuer Ansatz mit Vater-Kategorie
                    $parent = $spalte - 1;

                    $_category = Mage::getResourceModel('catalog/category_collection')
                            ->addFieldToFilter('name', $data[$parent])
                            ->getFirstItem(); // The parent category

                    $categoryParentId = $_category->getId();
                    $category = Mage::getModel('catalog/category')->load($categoryParentId);

                    $_childcategory = $category->getChildrenCategories();

                    foreach ($_childcategory as $_cat) {
                        if ($_cat->getName() == $data[$spalte]) {
                            if ($_cat->getId() != "") {
                                $temp[$i] = array($data[0] => $_cat->getId());
                            }
                        }
                    }
                }
                $spalte++;
                $i++;
            }
        }
        $zeiger++;
    }
    $skus = array_unique($skus);

    foreach ($skus as $sku) {
        $categories = array();
        foreach ($temp as $row) {
            foreach ($row as $key => $value) {
                if ($key == $sku) {
                    array_push($categories, $value);
                }
            }
        }
        $categories = array_unique($categories);
        echo "Für " . $sku . " die Kategorien:<br>";
        echo "<pre>";
        print_r($categories);
        echo "</pre>";

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        if ($product) {
            echo $product->getName() . "<br>";
            $product->setCategoryIds(array($categories));
            $product->save();
        }
    }


    $class->closeFile($fileHandle);
} else {
    echo "Keine Input-Datei gefunden!!!";
}

$class->closeFile($logFileHandle);

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

}
