<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 27.11.2020
 * Time: 15:30
 * 
 * Nicht in der Datei jtl.csv vorhandene Artikel löschen
 * 
 */
//ini_set("memory_limit", '4G');
ini_set('max_execution_time', 3600);

$class = NEW SmsConvertMkz();
error_reporting(1);

define('IMPORT_FILE', 'artikel.csv');
define('IMPORT_PATH', '../../app/code/local/Sms/Artikelfinder/Import/');

define('LOG_FILE', 'mkz.log');
define('LOG_PATH', '../../var/log/');

$fileRowCount = 0;
$zeiger = 1;

$artikel = fopen("mkz_artikel_import.csv", "w+");

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {

        if ($zeiger > 1) {
            $bereinigt = substr($data[1], 1);
            $bereinigt = substr($bereinigt, 0, -1);
            $mkz = explode(';', $bereinigt);
            foreach ($mkz as $_mkz) {
                $zeile = array(
                    "",
                    $_mkz,
                    $data[0],
                    $data[5],
                    $data[4]
                );
                fputcsv($artikel, $zeile, ",");
            }
        }
        $zeiger++;
    }
    $class->closeFile($fileHandle);
} else {

    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}

$end = microtime(true);
$runtime = number_format($end - $start, 2);
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsConvertMkz {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
