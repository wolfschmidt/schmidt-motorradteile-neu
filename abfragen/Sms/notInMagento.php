<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 27.11.2020
 * Time: 15:30
 * 
 * Nicht in der Datei jtl.csv vorhandene Artikel löschen
 * 
 */
//ini_set("memory_limit", '4G');
ini_set('max_execution_time', 3600);
require_once '../../app/Mage.php';

UMASK(0);
Mage::app();
Mage::init();

Mage::register('isSecureArea', true); /* set secure admin area */

$class = NEW SmsConvertArtikelexport();
error_reporting(1);
$artikelfinder = $class->getAllItemsFromSms();
$fileRowCount = 0;
define('LOG_FILE', 'delete.log');
define('LOG_PATH', '../../var/log/');

$not_pawu = array();
$pawu_temp = fopen("pawu_artikel.csv", "r");
while (($pawu = fgetcsv($pawu_temp, 1000, ";")) !== FALSE) {
        array_push($not_pawu, $pawu[0]);
}

foreach ($artikelfinder as $smsArtikel) {
    $key = array_search($smsArtikel, $not_pawu);
    if ($key != 0) {
        echo $smsArtikel." ist vorhanden<br>";
    } else {
        echo $smsArtikel . " wurde gelöscht<br>";
        $delete = $class->deleteFromArtikelfinder($smsArtikel);
        $fileRowCount++;
    }
}

$collection = Mage::getModel('catalog/product')->getCollection();

//foreach ($artikelfinder as $smsArtikel) {
//    if (!in_array($smsArikel, $collection)) {
//        echo $smsArtikel . " ist nicht vorhanden<br>";
//        $fileRowCount++;
//    }
//}
Mage::unregister('isSecureArea'); /* un set secure admin area */
$end = microtime(true);
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

    function getAllItemsFromSms() {
        $read = $this->connect("read");
        $SQL = "SELECT artikelnr FROM sms_artikel_zu_fahrzeug GROUP BY artikelnr ORDER BY artikelnr";
        $result = $read->fetchAll($SQL);
        foreach ($result as $item) {
            $artikel[] = $item['artikelnr'];
        }
        return($artikel);
    }
    
    function deleteFromArtikelfinder ($artikelnr){
        $write = $this->connect("write");
        $SQL = "DELETE FROM sms_artikel_zu_fahrzeug WHERE artikelnr like '$artikelnr'";
        $delete = $write->query($SQL);
        $SQL2 = "DELETE FROM sms_artikel_verwendung WHERE artikelnr like '$artikelnr'";
        $delete´2 = $write->query($SQL2);
    }

    private function connect($kind) {
        if ($kind == "write") {
            $con = Mage::getSingleton('core/resource')->getConnection('core_write');
        } else {
            $con = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $con;
    }

}
