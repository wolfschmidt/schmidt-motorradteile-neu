<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 27.11.2020
 * Time: 15:30
 * 
 * Nicht in der Datei jtl.csv vorhandene Artikel löschen
 * 
 */
//ini_set("memory_limit", '4G');
ini_set('max_execution_time', 3600);

require_once '../../app/Mage.php';
Mage::app();

$class = NEW SmsImportMkz();
error_reporting(1);
$verwendung = $_SERVER['argv'][1];

switch ($verwendung) {
    case "vorn":
        define('IMPORT_FILE', 'verwendungvorn_artikel.csv');
        break;
    case "hinten":
        define('IMPORT_FILE', 'verwendung_hinten_artikel.csv');
        break;
    default:
        define('IMPORT_FILE', 'mkz_hinten_artikel_import.csv');
        break;
}

define('IMPORT_PATH', '');
$fileRowCount = 0;

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {
        if ($data[1] != "") {
            switch ($verwendung) {
                case "vorn":
                    $class->insertDataVerwendung($data[1], $data[2], $data[3], $data[4], $data[5]);
                    break;
                case "hinten":
                    $class->insertDataVerwendung($data[1], $data[2], $data[3], $data[4], $data[5]);
                    break;
                default:
                    $class->insertData($data[1], $data[2], $data[3], $data[4]);
                    break;
            }
            $fileRowCount++;
            echo $data[2] . " eingefügt mit Verwendung " . $verwendung . "\n";
        }
    }
    $class->closeFile($fileHandle);
} else {
    echo "Hallo??? " . IMPORT_PATH . IMPORT_FILE . "<br>";
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}

$end = microtime(true);
$runtime = number_format($end - $start, 2);
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsImportMkz {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

    public function insertData($fahrzeug_id, $artikelnr, $art_cat, $art_subcat) {
        $con = $this->dbConnect("read");
        $execute = $this->dbConnect("write");
        $pruefen = "SELECT id FROM sms_artikel_zu_fahrzeug WHERE fahrzeug_id = '$fahrzeug_id' AND artikelnr = '$artikelnr'";
        $result = $con->fetchAll($pruefen);

        if (count($result) == 0) {
            $sql = "INSERT INTO sms_artikel_zu_fahrzeug (fahrzeug_id, artikelnr, art_cat, art_subcat)"
                    . "VALUES"
                    . "('$fahrzeug_id', '$artikelnr', '$art_cat', '$art_subcat')";
            $insert = $execute->query($sql);
        }
    }

    public function insertDataVerwendung($fahrzeug_id, $artikelnr, $verwendung, $anzahl, $fuellmenge) {
        $con = $this->dbConnect("read");
        $execute = $this->dbConnect("write");
        $pruefen = "SELECT id FROM sms_artikel_verwendung WHERE fahrzeug_id = '$fahrzeug_id' AND artikelnr = '$artikelnr' AND verwendung = '$verwendung'";
        $result = $con->fetchAll($pruefen);
        if (count($result) == 0) {
            $sql = "INSERT INTO sms_artikel_verwendung (fahrzeug_id, artikelnr, verwendung, anzahl, fuellmenge)"
                    . "VALUES"
                    . "('$fahrzeug_id', '$artikelnr', '$verwendung', '$anzahl', '$fuellmenge')";
            $insert = $execute->query($sql);
        }
    }

    function dbConnect($art) {
        $coreResource = Mage::getSingleton('core/resource');
        if ($art == "write") {
            $con = $coreResource->getConnection('core_write');
        } else {
            $con = $coreResource->getConnection('core_read');
        }
        return $con;
    }

}
