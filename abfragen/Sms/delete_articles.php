<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 27.11.2020
 * Time: 15:30
 * 
 * Nicht in der Datei jtl.csv vorhandene Artikel löschen
 * 
 */
//ini_set("memory_limit", '4G');
ini_set('max_execution_time', 3600);
require_once '../../app/Mage.php';

UMASK(0);
Mage::app();
Mage::init();

Mage::register('isSecureArea', true); /* set secure admin area */
//Mage::getModel('review/review')->load($review)->delete(); /* Your operation */

$class = NEW SmsConvertArtikelexport();
error_reporting(1);

define('IMPORT_FILE', 'jtl.csv');
define('IMPORT_PATH', '../');

define('LOG_FILE', 'delete.log');
define('LOG_PATH', '../../var/log/');

$fileRowCount = 0;
$jtl = array();

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        array_push($jtl, $data[0]);
    }
    $class->closeFile($fileHandle);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$collection = Mage::getModel('catalog/product')->getCollection();

foreach ($collection as $product) {
    if (!in_array($product->getSku(), $jtl)) {
        echo $product->getSku() . " wurde gelöscht<br>";
        $product->delete();
    }
}
Mage::unregister('isSecureArea'); /* un set secure admin area */


$end = microtime(true);

$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
