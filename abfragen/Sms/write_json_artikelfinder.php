<?php

/**
 * Author: Wolf Schmidt, Schmidt Medienservice, www.schmidt-medien.de
 * Date: 30.09.2019
 * Time: 14:00
 */
require_once '../../app/Mage.php';
Mage::app();
//$neuheiten = new Sms_Neuheiten_Helper_Data();
$resource = Mage::getSingleton('core/resource');
$write = $resource->getConnection('core_write');
$read = $resource->getConnection('core_read');

//$truncateCountries = $write->query("TRUNCATE sms_neuheiten_country");
//$truncateCities = $write->query("TRUNCATE sms_neuheiten_cities");
//
//$collection = Mage::getModel('catalog/product')->getCollection();
//$collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
//$collection->addAttributeToSelect(array('country', 'village'))
//        ->addAttributeToFilter(array(
//            array(
//                'attribute' => 'country',
//                'neq' => ''
//            ),
//        ));
//
//
//foreach ($collection as $_product) {
//    $product = Mage::getModel('catalog/product')->load($_product->getId());
//    $_country = $product->getCountry();
//    $_city = $product->getVillage();
//    $city = addslashes($_city);
//
//    $categories = $product->getCategoryIds();
//    foreach ($categories as $cat) {
//        $_gibtes = "SELECT country_code FROM sms_neuheiten_country WHERE country_code like '$_country' AND catId = '$cat'";
//        $_result = $read->fetchCol($_gibtes);
//        if (count($_result) == 0) {
//            $newresult = $write->query("INSERT INTO sms_neuheiten_country (id,country_code,catId)
//            VALUES
//            ('NULL','$_country', '$cat');");
//        }
//        $_check = "SELECT city FROM sms_neuheiten_cities WHERE city like '$city' AND countryCode like '$_country' AND catId = '$cat'";
//        $_checkresult = $read->fetchCol($_check);
//        if (count($_checkresult) == 0) {
//            $newresult = $write->query("INSERT INTO sms_neuheiten_cities (id,city,countryCode,catId)
//            VALUES
//            ('NULL','$city', '$_country', '$cat');");
//        }
//    }
//}

// Ab hier write Json //
$getallHersteller = "Select hersteller FROM sms_fahrzeug;";
$allHersteller = $read->fetchCol($getallHersteller);
$_allHersteller = array_unique($allHersteller);
$anzahlHersteller = count($_allHersteller);
$i = 1;

foreach ($_allHersteller as $key => $value) {
    $getallHubraum = "SELECT hubraum FROM sms_fahrzeug WHERE hersteller like '$value'";
    $allHubraum = $read->fetchCol($getallHubraum);
    $_allHubraum = array_unique($allHubraum);
    asort($_allHubraum);

    $anzahlHubraum = count($_allHubraum);
    $k = 1;
    $HerstellerHubraum = $HerstellerHubraum . "\"". $value . "\": \"";
    foreach ($_allHubraum as $key => $value2) {
        if ($k<$anzahlHubraum) {
            $HerstellerHubraum = $HerstellerHubraum . $value2 . ",";
        } else {
            $HerstellerHubraum = $HerstellerHubraum . $value2;
        }
        $k++;
    }
    if ($i<$anzahlHersteller) {
        $HerstellerHubraum = $HerstellerHubraum . "\",\n";
    } else {
        $HerstellerHubraum = $HerstellerHubraum . "\"";
    }
    $i++;
}


$pre = "{\n";
$post = "\n}";
$datei = fopen("../../media/sms/modelle.json", "w");
$ergebnis = $pre . $HerstellerHubraum . $post;
fwrite($datei, $ergebnis);
fclose($datei);

// Ab hier write Json for category 4 and 26 //
//$getallCatCountries = "Select countryCode FROM sms_neuheiten_cities WHERE (catId = 4 OR catId = 26);";
//$allCatCountries = $read->fetchCol($getallCatCountries);
//$_allCatCountries = array_unique($allCatCountries);
//$anzahlCatCountries = count($_allCatCountries);
//$i = 1;
//
//foreach ($_allCatCountries as $key => $value3) {
//    $getallCatCities = "SELECT city FROM sms_neuheiten_cities WHERE countryCode like '$value3' AND (catId = 4 OR catId = 26)";
//    $allCatCities = $read->fetchCol($getallCatCities);
//    $_allCatCities = array_unique($allCatCities);
//    asort($_allCatCities);
//    $anzahlCatCities = count($_allCatCities);
//    $k = 1;
//
//    $CountryCatCity = $CountryCatCity . "\"". $value3 . "\": \"";
//    foreach ($_allCatCities as $key => $value4) {
//        if ($k<$anzahlCatCities) {
//            $CountryCatCity = $CountryCatCity . $value4 . ",";
//        } else {
//            $CountryCatCity = $CountryCatCity . $value4;
//        }
//        $k++;
//    }
//    if ($i<$anzahlCatCountries) {
//        $CountryCatCity = $CountryCatCity . "\",\n";
//    } else {
//        $CountryCatCity = $CountryCatCity . "\"";
//    }
//    $i++;
//}
//
//
//$pre = "{\n";
//$post = "\n}";
//$dateiCat = fopen("../media/sms/dataCat4.json", "w");
//$ergebnis = $pre . $CountryCatCity . $post;
//fwrite($dateiCat, $ergebnis);
//fclose($dateiCat);
//
//// Ab hier write Json for category 19, 20, 21 and 27 //
//$getallCat19Countries = "Select countryCode FROM sms_neuheiten_cities WHERE catId = 19 OR catId = 20 OR catId = 21 OR catId = 27;";
//$allCat19Countries = $read->fetchCol($getallCat19Countries);
//$_allCat19Countries = array_unique($allCat19Countries);
//$anzahlCat19Countries = count($_allCat19Countries);
//$i = 1;
//
//foreach ($_allCat19Countries as $key => $value3) {
//    $getallCat19Cities = "SELECT city FROM sms_neuheiten_cities WHERE countryCode like '$value3' AND (catId = 19 OR catId = 20 OR catId = 21 OR catId = 27)";
//    $allCat19Cities = $read->fetchCol($getallCat19Cities);
//    $_allCat19Cities = array_unique($allCat19Cities);
//    asort($_allCat19Cities);
//    $anzahlCat19Cities = count($_allCat19Cities);
//    $k = 1;
//
//    $CountryCat19City = $CountryCat19City . "\"". $value3 . "\": \"";
//    foreach ($_allCat19Cities as $key => $value4) {
//        if ($k<$anzahlCat19Cities) {
//            $CountryCat19City = $CountryCat19City . $value4 . ",";
//        } else {
//            $CountryCat19City = $CountryCat19City . $value4;
//        }
//        $k++;
//    }
//    if ($i<$anzahlCat19Countries) {
//        $CountryCat19City = $CountryCat19City . "\",\n";
//    } else {
//        $CountryCat19City = $CountryCat19City . "\"";
//    }
//    $i++;
//}
//
//
//$pre = "{\n";
//$post = "\n}";
//$dateiCat = fopen("../media/sms/dataCat19.json", "w");
//$ergebnis = $pre . $CountryCat19City . $post;
//fwrite($dateiCat, $ergebnis);
//fclose($dateiCat);
//
//// Ab hier write Json for category 11, 23 and 24 //
//$getallCat11Countries = "Select countryCode FROM sms_neuheiten_cities WHERE catId = 11 OR catId = 23 OR catId = 24;";
//$allCat11Countries = $read->fetchCol($getallCat11Countries);
//$_allCat11Countries = array_unique($allCat11Countries);
//$anzahlCat11Countries = count($_allCat11Countries);
//$i = 1;
//
//foreach ($_allCat11Countries as $key => $value3) {
//    $getallCat11Cities = "SELECT city FROM sms_neuheiten_cities WHERE countryCode like '$value3' AND (catId = 11 OR catId = 23 OR catId = 24)";
//    $allCat11Cities = $read->fetchCol($getallCat11Cities);
//    $_allCat11Cities = array_unique($allCat11Cities);
//    asort($_allCat11Cities);
//    $anzahlCat11Cities = count($_allCat11Cities);
//    $k = 1;
//
//    $CountryCat11City = $CountryCat11City . "\"". $value3 . "\": \"";
//    foreach ($_allCat11Cities as $key => $value4) {
//        if ($k<$anzahlCat11Cities) {
//            $CountryCat11City = $CountryCat11City . $value4 . ",";
//        } else {
//            $CountryCat11City = $CountryCat11City . $value4;
//        }
//        $k++;
//    }
//    if ($i<$anzahlCat11Countries) {
//        $CountryCat11City = $CountryCat11City . "\",\n";
//    } else {
//        $CountryCat11City = $CountryCat11City . "\"";
//    }
//    $i++;
//}
//
//
//$pre = "{\n";
//$post = "\n}";
//$dateiCat = fopen("../media/sms/dataCat11.json", "w");
//$ergebnis = $pre . $CountryCat11City . $post;
//fwrite($dateiCat, $ergebnis);
//fclose($dateiCat);
//
//// Ab hier write Json for category 6 //
//$getallCat6Countries = "Select countryCode FROM sms_neuheiten_cities WHERE catId = 6;";
//$allCat6Countries = $read->fetchCol($getallCat6Countries);
//$_allCat6Countries = array_unique($allCat6Countries);
//$anzahlCat6Countries = count($_allCat6Countries);
//$i = 1;
//
//foreach ($_allCat6Countries as $key => $value3) {
//    $getallCat6Cities = "SELECT city FROM sms_neuheiten_cities WHERE countryCode like '$value3' AND (catId = 6)";
//    $allCat6Cities = $read->fetchCol($getallCat6Cities);
//    $_allCat6Cities = array_unique($allCat6Cities);
//    asort($_allCat6Cities);
//    $anzahlCat6Cities = count($_allCat6Cities);
//    $k = 1;
//
//    $CountryCat6City = $CountryCat6City . "\"". $value3 . "\": \"";
//    foreach ($_allCat6Cities as $key => $value4) {
//        if ($k<$anzahlCat6Cities) {
//            $CountryCat6City = $CountryCat6City . $value4 . ",";
//        } else {
//            $CountryCat6City = $CountryCat6City . $value4;
//        }
//        $k++;
//    }
//    if ($i<$anzahlCat6Countries) {
//        $CountryCat6City = $CountryCat6City . "\",\n";
//    } else {
//        $CountryCat6City = $CountryCat6City . "\"";
//    }
//    $i++;
//}
//
//
//$pre = "{\n";
//$post = "\n}";
//$dateiCat = fopen("../media/sms/dataCat6.json", "w");
//$ergebnis = $pre . $CountryCatCity . $post;
//fwrite($dateiCat, $ergebnis);
//fclose($dateiCat);
//
//// Ab hier write Json for category 7 //
//$getallCat7Countries = "Select countryCode FROM sms_neuheiten_cities WHERE catId = 7;";
//$allCat7Countries = $read->fetchCol($getallCat7Countries);
//$_allCat7Countries = array_unique($allCat7Countries);
//$anzahlCat7Countries = count($_allCat7Countries);
//$i = 1;
//
//foreach ($_allCat7Countries as $key => $value3) {
//    $getallCat7Cities = "SELECT city FROM sms_neuheiten_cities WHERE countryCode like '$value3' AND (catId = 7)";
//    $allCat7Cities = $read->fetchCol($getallCat7Cities);
//    $_allCat7Cities = array_unique($allCat7Cities);
//    asort($_allCat7Cities);
//    $anzahlCat7Cities = count($_allCat7Cities);
//    $k = 1;
//
//    $CountryCat7City = $CountryCat7City . "\"". $value3 . "\": \"";
//    foreach ($_allCat7Cities as $key => $value4) {
//        if ($k<$anzahlCat7Cities) {
//            $CountryCat7City = $CountryCat7City . $value4 . ",";
//        } else {
//            $CountryCat7City = $CountryCat7City . $value4;
//        }
//        $k++;
//    }
//    if ($i<$anzahlCat7Countries) {
//        $CountryCat7City = $CountryCat7City . "\",\n";
//    } else {
//        $CountryCat7City = $CountryCat7City . "\"";
//    }
//    $i++;
//}
//
//
//$pre = "{\n";
//$post = "\n}";
//$dateiCat = fopen("../media/sms/dataCat7.json", "w");
//$ergebnis = $pre . $CountryCatCity . $post;
//fwrite($dateiCat, $ergebnis);
//fclose($dateiCat);


function unique_multidim_array($array, $key)
{
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach ($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}