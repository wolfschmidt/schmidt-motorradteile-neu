<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 27.11.2020
 * Time: 15:30
 * 
 * Nicht in der Datei jtl.csv vorhandene Artikel löschen
 * 
 */
//ini_set("memory_limit", '4G');
ini_set('max_execution_time', 3600);

$class = NEW SmsConvertMkz();
error_reporting(1);

define('IMPORT_FILE', 'artikel.csv');
define('IMPORT_PATH', '../../app/code/local/Sms/Artikelfinder/Import/');

define('LOG_FILE', 'mkz.log');
define('LOG_PATH', '../../var/log/');

$fileRowCount = 0;
$zeiger = 1;

$artikel = fopen("mkz_hinten_artikel_import.csv", "w+");
$artikelverwendung = fopen("verwendung_hinten_artikel.csv", "w+");


if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {

        if ($zeiger > 1) {
            if ($data[2] != "" && $data[0] != "") {
                $str = $data[3];
                if ($str[0] == ";") {
                    $bereinigt = substr($data[3], 1);
                } else {
                    $bereinigt = $data[3];
                }
                $bereinigt = substr($bereinigt, 0, -1);
                $mkz = explode(';', $bereinigt);
                foreach ($mkz as $_mkz) {
                    $zeile = array(
                        "",
                        $_mkz,
                        $data[0],
                        $data[5],
                        $data[4]
                    );
                    $_verwend = array(
                        "",
                        $_mkz,
                        $data[0],
                        "hinten",
                        "",
                        ""
                    );
                    fputcsv($artikel, $zeile, ",");
                    fputcsv($artikelverwendung, $_verwend, ",");
                }
            }
        }
        $zeiger++;
    }
    $class->closeFile($fileHandle);
} else {

    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}

$end = microtime(true);
$runtime = number_format($end - $start, 2);
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsConvertMkz {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    public function deleteDoubleInserts() {
//        SELECT sazf.id, sazf.fahrzeug_id, sazf.artikelnr
//        FROM sms_artikel_zu_fahrzeug sazf
//        INNER JOIN (
//        SELECT fahrzeug_id, artikelnr
//        FROM sms_artikel_zu_fahrzeug
//        GROUP BY fahrzeug_id, artikelnr
//        HAVING COUNT(id) > 1) dup ON sazf.artikelnr = dup.artikelnr && sazf.fahrzeug_id = dup.fahrzeug_id;
////        
//        CREATE TABlE sms_dubletten IF NOT EXISTS   
//        (SELECT ID, fahrzeug_id, artikelnr
//FROM sms_artikel_zu_fahrzeug sazf
//WHERE EXISTS
//(SELECT * FROM sms_artikel_zu_fahrzeug Kopie WHERE sazf.fahrzeug_id = Kopie.fahrzeug_id AND sazf.artikelnr = Kopie.artikelnr AND sazf.id <> Kopie.id)
//ORDER BY artikelnr);
////            
//        DELETE FROM sms_artikel_zu_fahrzeug WHERE EXISTS
//        (SELECT * FROM sms_artikel_zu_fahrzeug Kopie
//        WHERE sms_artikel_zu_fahrzeug.fahrzeug_id = Kopie.fahrzeug_id
//        AND sms_artikel_zu_fahrzeug.artikelnr = Kopie.artikelnr
//        AND sms_artikel_zu_fahrzeug.id < Kopie.id)
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
