<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 04.08.2020
 * Time: 10:18
 */
$start = microtime(true);
$class = NEW SmsConvertArtikelexport();

define('IMPORT_FILE', 'HH_v2.csv');
define('IMPORT_PATH', '');



if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
//        $sub = substr($data[0], 0, 1);
//        $subsub = substr($data[0], 1, 1);
//        for ($i = 0; $i < 4; $i++) {
//            if ($i == 0) {
//                $bild = $data[0] . ".jpg";
//            } else {
//                $bild = $data[0] . "_" . $i . ".jpg";
//            }
//            $bildadresse = "https://pwonline.de/media/catalog/product/" . $sub . "/" . $subsub . "/" . $bild;            
//            $contents = file_get_contents($bildadresse);
//            $contents1 = str_replace("https://pwonline.de/media/catalog/product/".$sub."/".$subsub."/", "", $bildadresse);
//            $savefile = fopen("../media/import/" . $contents1, "w");
//            fwrite($savefile, $contents);
//            fclose($savefile);
//
//            echo $bildadresse . "<br>";
//        }
        for ($i = 1; $i < 5; $i++) {
            if ($data[$i] != "" && !empty($data[$i])) {
                $bildadresse = "https://pwonline.de/media/catalog/product/" . $data[$i];
                $contents = file_get_contents($bildadresse);
                $contents1 = $data[0] . "_" . $i . ".jpg";
                $savefile = fopen("F:\Ablage\Motorradservice Schmidt\Warenwirtschaft\Lieferanten\Bilder PaWu/Test/" . $contents1, "w");
                fwrite($savefile, $contents);
                fclose($savefile);

                echo $bildadresse . " => " . $saveFile . "\r\n";
            }
        }
    }
    $class->closeFile($fileHandle);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

    public function remoteFileExists($url) {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    }

}
