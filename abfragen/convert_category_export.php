<?php
/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 17.10.2017
 * Update 17.10.2017
 * Time: 11:18
 */
$start = microtime(true);
$class = NEW SmsConvertArtikelexport();

error_reporting(1);

define ('IMPORT_FILE', 'Artikelkategorien.csv');
define ('IMPORT_PATH', '../var/import/');

define ('LOG_FILE', 'artikeldaten.log');
define ('LOG_PATH', 'var/log/');

$handle = fopen("../var/import/artikelkategorien_convertiert.csv", w);
$fileRowCount = 0;

define ('ESCAPE', '|');



if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $fileRowCount++;
        $spalte = 0;
        echo $data[0] . "<br>" . $data[1]. "<br>";
        while($spalte < 5) {
            if ($spalte == 0 ) {
                fwrite($handle, ESCAPE.$data[$spalte].ESCAPE.";".ESCAPE);
            } else {

                if ($data[$spalte] != "") {
                    fwrite($handle, $data[$spalte] . "/");
                }
            }
            $spalte++;
        }
        fwrite($handle, ESCAPE."\r");
    }

    $class->closeFile($fileHandle);
    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);


class SmsConvertArtikelexport
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}