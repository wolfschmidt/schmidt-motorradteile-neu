<?php
/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 17.10.2017
 * Update 17.10.2017
 * Time: 11:18
 */
$start = microtime(true);
$class = NEW SmsConvertBildexport();


define ('IMPORT_FILE', 'Bildexport.csv');
define ('IMPORT_TEMP', 'artikelbilder_temp.csv');
define ('IMPORT_PATH', '../var/import/');

define ('LOG_FILE', 'bildimport.log');
define ('LOG_PATH', 'var/log/');

$temp = fopen("../var/import/artikelbilder_temp.csv", 'w');
$handle = fopen("../var/import/arikelbilder_convertiert.csv", 'w');
$fileRowCount = 0;

define ('ESCAPE', '"');
define ('TRENNER', ';');
$exclude = "Bild";


if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $fileRowCount++;
        $spalte = 0;
        while ($spalte < 7) {
            $pos = stripos($data[$spalte], $exclude);

            if ($data[$spalte] != "") {
                if ($spalte <= 1) {
                    if ($pos === false) {
                        if ($spalte == 1) {
                            fwrite($temp, ESCAPE.utf8_encode($data[$spalte]).ESCAPE.TRENNER.ESCAPE.utf8_encode($data[$spalte]).ESCAPE.TRENNER.ESCAPE.utf8_encode($data[$spalte]).ESCAPE.TRENNER);
                        } else {
                            fwrite($temp, ESCAPE.utf8_encode($data[$spalte]).ESCAPE.TRENNER);
                        }
                    }
                    if ($spalte == 1) {
                        if ($data[1] == "Bild 1") {
                            fwrite($temp, ESCAPE."image".ESCAPE.TRENNER.ESCAPE."small_image".ESCAPE.TRENNER.ESCAPE."thumbnail".ESCAPE.TRENNER);
                        }
                        if ($data[4] == "Bild 4") {
                            fwrite($temp, ESCAPE."media_gallery".ESCAPE);
                        }
                    }
                }

                if ($spalte > 0 && $pos === false) {
                    fwrite($temp, ESCAPE.utf8_encode($data[$spalte]).";/");
                }
            }
            $spalte++;
        }
        fwrite($temp, "\n");
    }

    $class->closeFile($fileHandle);

    //    Ab hier die echte Datei formatieren

    $convert = file(IMPORT_PATH.IMPORT_TEMP);
    for($i=0;$i < count($convert); $i++){
        if ($i > 0) {
        $convert[$i] = str_replace(';/"',';/',$convert[$i]);
//        $endezeichen ='"';
//    $convert[$i] .= '"';
        }
        echo $i.": ".$convert[$i]."<br>";
        fwrite($handle, $convert[$i]);
    }

//    if (($fileHandle2 = $class->openFile(IMPORT_PATH, IMPORT_TEMP, 'r')) !== FALSE) {
//        while (($data = fgetcsv($fileHandle2, 1000, TRENNER)) !== FALSE) {
//            $spalte_2 = 0;
//
//            while ($spalte_2 < 5) {
////                if ($data[$spalte_2] != "") {
//                fwrite($handle, ESCAPE . $data[$spalte_2] . ESCAPE . TRENNER);
////                }
//                echo "Spalte ".$spalte_2." = ".$data[$spalte_2]. " ".TRENNER." ";
//                $spalte_2++;
//            }
//            echo "<br>";
//            fwrite($handle, "\n");
//        }
//    }
//
//    $class->closeFile($fileHandle2);

    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->printMessage($message);



class SmsConvertBildexport
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}