<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 04.08.2020
 * Time: 10:18
 * 
 * Prüfen ob die in JTL angelegten PW Artikel noch in der lagermenge.txt vorhanden sind. Wenn nicht schreibe die delete2.csv für den Import in JTL
 */
$start = microtime(true);
$class = NEW SmsConvertArtikelexport();

error_reporting(1);

define('IMPORT_FILE', 'lagermenge.txt');
define('IMPORT_PATH', '');
define('VERGLEICH', 'jtl.csv');

define('LOG_FILE', 'jtl.log');
define('LOG_PATH', 'var/log/');

$fileRowCount = 0;

$jtl = array();
$loeschen = "delete2.csv";
if (file_exists($loeschen)) {
    unlink($loeschen);
    } else {
echo "Die Datei $loeschen existiert nicht.";
}
if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        array_push($jtl, $data[0]);
    }

    $class->closeFile($fileHandle);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}

if (($fileHandle2 = $class->openFile(IMPORT_PATH, VERGLEICH, 'r')) !== FALSE) {
    while (($data2 = fgetcsv($fileHandle2, 1000, ";")) !== FALSE) {
        $fileRowCount++;
        $spalte = 0;
        if (!in_array($data2[0], $jtl)) {
            $inactive = fopen('delete2.csv', 'a');

        $del = array($data2[0],"N","N");
        print_r($del);
//            echo $delete."<br>";
            
            $spalte++;
            fputcsv($inactive, $del);
                fclose($inactive);
        }
    }
    $class->closeFile($fileHandle2);
    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
