<?php
/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 17.10.2017
 * Update 17.10.2017
 * Time: 11:18
 */
$start = microtime(true);
$class = NEW SmsConvertArtikelexport();

//ini_set('memory_limit','4096M');
//ini_set('display_errors', '1');
error_reporting(1);

define ('IMPORT_FILE', 'Artikeldaten.csv');
define ('IMPORT_PATH', '../var/import/');

define ('LOG_FILE', 'artikeldaten.log');
define ('LOG_PATH', 'var/log/');

$handle = fopen("../var/import/artikeldaten_convertiert.csv", w);
$fileRowCount = 0;

define ('ESCAPE', '|');



if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $fileRowCount++;
        $spalte = 0;
//        if ($data[12] >= 1) { $data[12] = 1; }
        echo $data[18] . "<br>" . $data[12]. "<br>";
        while($spalte < 28) {
            if ($spalte < 18 ) {
                fwrite($handle, ESCAPE.$data[$spalte].ESCAPE.";");
            } else {
                if ($spalte == 18) {
                    fwrite($handle, ESCAPE);
                }
                if ($data[$spalte] != "") {
                    fwrite($handle, $data[$spalte] . "/");

                }
            }
            $spalte++;
        }
        fwrite($handle, ESCAPE."\r");
    }

    $class->closeFile($fileHandle);
    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->writeLogEntry($message, $logFileHandle);
$class->printMessage($message);

$class->closeFile($logFileHandle);


class SmsConvertArtikelexport
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}