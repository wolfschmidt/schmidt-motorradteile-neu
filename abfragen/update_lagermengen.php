<?php

/**
 * Created with PhpStorm 10.0
 * Autor: Schmidt Medienservice, Wolf Schmidt | Foto-Stendel, Stephan Stendel
 * für Paaschburg & Wunderlich
 * 2016-02-25
 * alle Rechte liegen beim Autor
 */
$start = microtime(true);
$class = NEW UpdateLagermengen();
ini_set('max_execution_time', 14400);
ini_set('memory_limit', '4G');
ini_set('display_errors', '1');
error_reporting(1);

$timestamp = time();
$datum = date("Ymd-H-i", $timestamp);
define('LOG_FILE', $datum.'_lagermengen_update.log');
define('LOG_PATH', '../var/log/');
require_once '../app/Mage.php';
Mage::app();

$logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'lieferzeit_it');
$coreResource = Mage::getSingleton('core/resource');
$dbWrite = $coreResource->getConnection('core_write');
$dbRead = $coreResource->getConnection('core_read');

$processCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
foreach ($processCollection as $process) {
    $process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
}

if (($fileHandle = fopen('lagermenge.txt', 'r')) !== FALSE) {

    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
        $zeile++;
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data[0]);
        if ($product) {
            $edit++;

            $query_id = "SELECT entity_id FROM catalog_product_entity WHERE sku like '$data[0]' LIMIT 1";
            $item_id = $dbRead->fetchOne($query_id);

            $sql = "UPDATE cataloginventory_stock_item SET qty = '$data[1]', is_in_stock = 1 "
                    . "WHERE cataloginventory_stock_item.product_id = '$item_id'";
            $isinstockUpdate = $dbWrite->query($sql);

            $sql2 = "UPDATE cataloginventory_stock_status SET stock_status = '0', qty = '$data[1]' "
                    . "WHERE cataloginventory_stock_status.product_id = '$item_id'";
            $isinstockUpdate2 = $dbWrite->query($sql2);


            $attvorhanden = "SELECT * FROM catalog_product_entity_varchar WHERE attribute_id ='$attributeId' AND entity_id = '$item_id'";
            $result = $dbRead->fetchAll($attvorhanden);
            if ($result) {
                $sql3 = "UPDATE catalog_product_entity_varchar SET value = '$data[2]' WHERE catalog_product_entity_varchar.entity_id = '$item_id'"
                        . " AND catalog_product_entity_varchar.attribute_id = '$attributeId'";
            } else {
                $sql3 = "INSERT INTO catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) "
                        . "VALUES ('4', '$attributeId', '0', '$item_id', '$data[2]') ";
            }
            $isinstockUpdate3 = $dbWrite->query($sql3);
            echo $data[0] . " => ";
            echo $data[1] . " ist die Lagermenge und " . $data[2] . " ist die lieferzeit\r\n";
        } else {
            echo $data[0] . " => Produkt nicht vorhanden - wird übersprungen\r\n";
            $class->writeLogEntry($data[0] . "Produkt nicht vorhanden", $fileHandle);
        }
    }
    $class->closeFile($fileHandle);
    $end = microtime(true);
} else {
    $class->writeLogEntry("Keine Input-Datei gefunden!!!", $logFileHandle);
}
$runtime = number_format($end - $start, 2);

$class->writeLogEntry("Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $zeile . " Zeilen eingelesen und " . $edit . " Änderungen vorgenommen.\n", $logFileHandle);
$class->closeFile($logFileHandle);

$processCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
foreach ($processCollection as $process) {
    $process->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
}

class UpdateLagermengen {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

}
