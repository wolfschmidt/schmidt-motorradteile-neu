<?php

/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 04.08.2020
 * Time: 10:18
 */
ini_set('max_execution_time', 900);
ini_set('memory_limit', '2G');
ini_set('display_errors', '1');
error_reporting(1);
$class = NEW SmsConvertArtikelexport();

define('LOG_FILE', 'nicht-vorhandene-artikel.log');
define('LOG_PATH', '../var/log/');

$logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');

file_put_contents("lagermenge_temp.txt", file_get_contents("https://pwonline.de/quelle/lagermenge.txt"));

$skus = array();
$jtl_temp = fopen("jtl_products.csv", "r");
while (($jtl = fgetcsv($jtl_temp, 1000, ";")) !== FALSE) {
    array_push($skus, $jtl[0]);
}

$class->closeFile($jtl_temp);

$temp = fopen("lagermenge_temp.txt", "r+");
$deliverytime = fopen("lagermenge.txt", "w+");
$header = array('sku', 'qty', 'lieferzeit_it', 'zeLh');
fputcsv($deliverytime, $header, ";");

while (($data = fgetcsv($temp, 1000, ";")) !== FALSE) {
    if (in_array($data[0], $skus)) {
        echo $data[0] . " - " . $data[1] . "<br>";
        $_delivery = array(
            $data[0],
            $data[1],
            $data[2],
            'Y'
        );
        fputcsv($deliverytime, $_delivery, ";");
    } else {
        $logMessage = $data[0]." ist nicht im Portfolio\r\n";
        $class->writeLogEntry($logMessage, $logFileHandle);
    }
}

$class->closeFile($deliverytime);
$class->closeFile($temp);

class SmsConvertArtikelexport {

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param) {
        return fopen($path . $fileHandle, $param);
    }

    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle) {
        return fclose($fileHandle);
    }

    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle) {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }

    /**
     * @param $message
     */
    public function printMessage($message) {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}
