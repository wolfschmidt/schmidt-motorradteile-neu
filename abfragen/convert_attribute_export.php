<?php
/**
 * Created by PhpStorm.
 * User: Wolf Schmidt - Schmidt Medienservice
 * Date: 23.10.2017
 * Update 17.10.2017
 * Time: 11:18
 */
$start = microtime(true);
$class = NEW SmsConvertAttribute();


define ('IMPORT_FILE', 'Attribute.csv');
define ('IMPORT_PATH', '../var/import/');
define ('ESCAPE', '"');
define ('TRENNER', ';');

$farbe = fopen("../var/import/attribute_farbe.csv", 'w');
fwrite($farbe, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."color".ESCAPE . "\n");

$material = fopen("../var/import/attribute_material.csv", 'w');
fwrite($material, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."material".ESCAPE . "\n");

$ausfuehrung = fopen("../var/import/attribute_ausfuehrung.csv", 'w');
fwrite($ausfuehrung, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."ausfuehrung".ESCAPE . "\n");

$hoehe = fopen("../var/import/attribute_hoehe.csv", 'w');
fwrite($hoehe, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."height".ESCAPE . "\n");

$breite = fopen("../var/import/attribute_breite.csv", 'w');
fwrite($breite, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."width".ESCAPE . "\n");

$tiefe = fopen("../var/import/attribute_tiefe.csv", 'w');
fwrite($tiefe, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."depth".ESCAPE . "\n");

$laenge = fopen("../var/import/attribute_laenge.csv", 'w');
fwrite($laenge, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."length".ESCAPE . "\n");

$durchmesser = fopen("../var/import/attribute_durchmesser.csv", 'w');
fwrite($durchmesser, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."durchmesser".ESCAPE . "\n");

$innendurchmesser = fopen("../var/import/attribute_innendurchmesser.csv", 'w');
fwrite($innendurchmesser, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."innendurchmesser".ESCAPE . "\n");

$oberflaeche = fopen("../var/import/attribute_oberflaeche.csv", 'w');
fwrite($oberflaeche, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."surface".ESCAPE . "\n");

$gewinde = fopen("../var/import/attribute_gewinde.csv", 'w');
fwrite($gewinde, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."gewinde".ESCAPE . "\n");

$glas = fopen("../var/import/attribute_glas.csv", 'w');
fwrite($glas, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."glas".ESCAPE . "\n");

$leuchtmittel = fopen("../var/import/attribute_leuchtmittel.csv", 'w');
fwrite($leuchtmittel, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."leuchtmittel".ESCAPE . "\n");

$gutachten = fopen("../var/import/attribute_gutachten.csv", 'w');
fwrite($gutachten, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."gutachten".ESCAPE . "\n");

$verwendung = fopen("../var/import/attribute_verwendung.csv", 'w');
fwrite($verwendung, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."verwendung".ESCAPE . "\n");

$halo = fopen("../var/import/attribute_halo.csv", 'w');
fwrite($halo, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."halo_featured".ESCAPE . "\n");

$neu_von = fopen("../var/import/attribute_neu_von.csv", 'w');
fwrite($neu_von, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."news_from_date".ESCAPE . "\n");

$neu_bis = fopen("../var/import/attribute_neu_bis.csv", 'w');
fwrite($neu_bis, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."news_to_date".ESCAPE . "\n");

$fahrzeugmarke = fopen("../var/import/attribute_fahrzeugmarke.csv", 'w');
fwrite($fahrzeugmarke, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."vehicle_manufacturer".ESCAPE . "\n");

$watt = fopen("../var/import/attribute_watt.csv", 'w');
fwrite($watt, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."watt".ESCAPE . "\n");

$volt = fopen("../var/import/attribute_volt.csv", 'w');
fwrite($volt, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."volt".ESCAPE . "\n");

$kapazitaet = fopen("../var/import/attribute_kapazitaet.csv", 'w');
fwrite($kapazitaet, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."kapazitaet".ESCAPE . "\n");

$form = fopen("../var/import/attribute_form.csv", 'w');
fwrite($form, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."form".ESCAPE . "\n");

$oem = fopen("../var/import/attribute_oem.csv", 'w');
fwrite($oem, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."oem".ESCAPE . "\n");

$kettenteilung = fopen("../var/import/attribute_kettenteilung.csv", 'w');
fwrite($kettenteilung, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."kettenteilung".ESCAPE . "\n");

$zaehnezahl = fopen("../var/import/attribute_zaehnezahl.csv", 'w');
fwrite($zaehnezahl, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."zaehneanzahl".ESCAPE . "\n");

$lochkreis = fopen("../var/import/attribute_lochkreis.csv", 'w');
fwrite($lochkreis, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."lochkreis".ESCAPE . "\n");

$bohrungen = fopen("../var/import/attribute_bohrungen.csv", 'w');
fwrite($bohrungen, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."bohrungen".ESCAPE . "\n");

$fuellmenge = fopen("../var/import/attribute_fuellmenge.csv", 'w');
fwrite($fuellmenge, ESCAPE."sku".ESCAPE.TRENNER.ESCAPE."fuellmenge".ESCAPE . "\n");

$fileRowCount = 0;

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {

        $fileRowCount++;
        if ($fileRowCount > 1) {
            if ($data[1] == utf8_decode("Glas")) {
                fwrite($glas, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Leuchtmittel")) {
                fwrite($leuchtmittel, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Gutachten")) {
                fwrite($gutachten, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Verwendung")) {
                fwrite($verwendung, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Farbe")) {
                fwrite($farbe, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Material")) {
                fwrite($material, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Ausführung") OR $data[1] == utf8_decode("AusfÃ¼hrung")) {
                fwrite($ausfuehrung, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Höhe")) {
                fwrite($hoehe, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Breite")) {
                fwrite($breite, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Tiefe")) {
                fwrite($tiefe, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Länge")) {
                fwrite($laenge, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Durchmesser")) {
                fwrite($durchmesser, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Innendurchmesser")) {
                fwrite($innendurchmesser, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Gewinde")) {
                fwrite($gewinde, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Oberfläche")) {
                fwrite($oberflaeche, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Halo Featured")) {
                fwrite($halo, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Neu von")) {
                fwrite($neu_von, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Neu bis")) {
                fwrite($neu_bis, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Fahrzeugmarke")) {
                fwrite($fahrzeugmarke, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Watt")) {
                fwrite($watt, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Volt")) {
                fwrite($volt, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Kapazität")) {
                fwrite($kapazitaet, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Form")) {
                fwrite($form, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("OEM Nummer (nur zu Vergleichszwecken)")) {
                fwrite($oem, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Kettenteilung")) {
                fwrite($kettenteilung, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Zähneanzahl")) {
                fwrite($zaehnezahl, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Lochkreis")) {
                fwrite($lochkreis, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Anzahl Bohrungen")) {
                fwrite($bohrungen, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
            if ($data[1] == utf8_decode("Füllmenge")) {
                fwrite($fuellmenge, ESCAPE.$data[0].ESCAPE.TRENNER.ESCAPE.utf8_encode($data[2]).ESCAPE."\n");
                echo $data[0] . TRENNER . $data[2] . "<br>";
            }
        }
    }
}

$end = microtime(true);

$runtime = number_format($end - $start, 2);
$fileRowCount = $fileRowCount - 1;
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen.\n";
$class->printMessage($message);



class SmsConvertAttribute
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message;
    }

}